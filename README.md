# BiblioTagger

Browser extension for tagging websites with the topics that they address.

## Development

1. Clone this repo
1. `cd` into `extension/`.
1. `npm install`
1. Run `npm run start` to start the development server.

## Install into Chrome

1. Clone this repo
1. `cd` into `extension/`.
1. `npm install`
1. `npm run build`
    - A new directory will be created at `extension/build`. This contains
        the artifacts that should be loaded into Chrome as the extension.
1. In Chrome, open chrome://extensions/.
1. Activate Developer mode.
1. Click "Load Unpacked"
1. Select the build directory that was created.

## Install into Firefox

Not yet implemented.
