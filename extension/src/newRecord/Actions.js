
/**
 * Actions that can be used to add a new record.
 */

const ACTION_NS = "record_builder";
export const RELATE_TO = `${ACTION_NS}/relates_to`;
export const ADD_TITLE = `${ACTION_NS}/add_title`;
export const ADD_LINK = `${ACTION_NS}/add_link`;
export const ADD_DESC = `${ACTION_NS}/add_desc`;
export const SUBMIT_DATA = `${ACTION_NS}/submit_data`;
export const DATA_SUBMITTED = `${ACTION_NS}/data_submitted`;
export const CLEAR_FORM = `${ACTION_NS}/clear_form`;
export const TOGGLE_TAG = `${ACTION_NS}/toggle_tag`;
export const LOAD_ASSOCIATED_TAGS = `${ACTION_NS}/load_associated_tags`;


/**
 * State that the data-under-preperation for this record
 * should _edit_ a record instead of creating a new one.
 * 
 * `id` is the ID of the record that should be edited.
 */
export function relateTo(id) {
  return {
    type: RELATE_TO,
    payload: id
  }
}

/**
 * State that the data-under-preperation for this record
 * should have its title updated.
 * 
 * `title` is the new data that should be used.
 */
export function addTitle(title) {
  if(title.length === 0) {
    // Title is the empty string. Save `null`
    // instead.
    title = null;
  }

  return {
    type: ADD_TITLE,
    payload: title
  }
}


/**
 * State that the data-under-preperation for this record
 * should have its link updated.
 * 
 * `link` is the new data that should be used.
 */
export function addLink(link) {
  if(link.length === 0) {
    // Link is the empty string. Save `null`
    // instead.
    link = null;
  }

  return {
    type: ADD_LINK,
    payload: link
  }
}


/**
 * State that the data-under-preperation for this record
 * should have its description updated.
 * 
 * `desc` is the new data that should be used.
 */
export function addDescription(desc) {
  return {
    type: ADD_DESC,
    payload: desc
  }
}


/**
 * A new record is ready to be persisted. Announce that we're
 * ready for submission.
 */
export function submitRecord() {
  return {
    type: SUBMIT_DATA,
    readyForSubmission: true
  }
}


/**
 * The new record has been persisted.
 */
export function confirmSubmission() {
  return {
    type: DATA_SUBMITTED
  }
}


/**
 * Reset the form.
 */
export function clearForm() {
  return {
    type: CLEAR_FORM
  }
}


/**
 * State to the builder that a tag should be toggled for the
 * data-under-prep.
 * 
 * `tagId` is the ID of the tag that should be related.
 */
export function toggleTag(tagId) {
  return {
    type: TOGGLE_TAG,
    tagId
  }
}


/**
 * Give the builder the list of tags that are currently associated
 * with this record.
 * 
 * `tagIds` is a list IDs of the tag that should be related.
 */
export function loadTags(tagIds) {
  return {
    type: LOAD_ASSOCIATED_TAGS,
    currentTags: tagIds
  }
}
