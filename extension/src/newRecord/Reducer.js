
/**
 * Reducer that handles building data for a new record.
 */

import {
  RELATE_TO,
  SUBMIT_DATA,
  DATA_SUBMITTED,
  ADD_DESC,
  ADD_LINK,
  CLEAR_FORM,
  ADD_TITLE,
  TOGGLE_TAG,
  LOAD_ASSOCIATED_TAGS
} from './Actions';

var initialState = {
  // If we're updating a record, this will be set to the ID of that
  // record. If this is a new record, this will be null.
  relatesToRecordId: null,

  // Information for this record.
  title: null,
  url: null,
  desc: "",

  // List of tags that are already related to this record.
  currentTags: [],
  // List of changes that should be made to the tags that are related to this record.
  addedTags: [],
  droppedTags: [],

  // Set to true when the data are ready to be persisted.
  readyForSubmission: false
}

function recordBuilderReducer(state=initialState, action) {
  switch (action.type) {
    case RELATE_TO:
      return {
        ...state,
        relatesToRecordId: action.payload
      }
    case ADD_TITLE:
      return {
        ...state,
        title: action.payload
      }
    case ADD_LINK:
      return {
        ...state,
        url: action.payload
      }
    case ADD_DESC:
      return {
        ...state,
        desc: action.payload
      }
    case SUBMIT_DATA:
      return {
        ...state,
        readyForSubmission: true
      }
    case DATA_SUBMITTED:
      // Data have been fully persisted. Reset the form.
      return {
        ...initialState
      }
    case CLEAR_FORM:
      // Reset the form.
      return {
        ...initialState
      }
    case TOGGLE_TAG:
      // Add or remove a tag from the record that's being prepared.
      var newAddedTags = state.addedTags;
      var newDroppedTags = state.droppedTags;

      if(state.currentTags.includes(action.tagId)) {
        // This tag is currently linked to this record in the saved data.

        if(state.droppedTags.includes(action.tagId)) {
          // The user has previously stated that this tag should get dropped
          // when the data are saved.
          // Action: Remove from list of tags that should be dropped.
          newDroppedTags = [...state.droppedTags.filter((tagId) => tagId === action.tagId)];
        } else {
          // Action: Add to list of tags that should be dropped.
          newDroppedTags = [...state.droppedTags, action.tagId];
        }
      } else {
        // This tag is not currently linked to this record in the saved data.

        if(state.addedTags.includes(action.tagId)) {
          // The user has previously stated that this tag should get linked
          // when the data are saved.
          // Action: Remove from list of tags that should be added.
          newAddedTags = [...state.addedTags.filter((tagId) => tagId !== action.tagId)];
        } else {
          // Action: Add to list of tags that should be added.
          newAddedTags = [...state.addedTags, action.tagId];
        }
      }

      return {
        ...state,
        addedTags: newAddedTags,
        droppedTags: newDroppedTags,
      }
    case LOAD_ASSOCIATED_TAGS:
      return {
        ...state,
        currentTags: action.currentTags
      }
    default:
      return state
  }
}

/**
 * Returns the list of tags that are related to
 * a record.
 * 
 * `recordBuilderState` is the current state of the record builder.
 * `recordId` is the ID of the record.
 */
export function selectTags(recordBuilderState) {
  return [
    ...recordBuilderState.currentTags,
    ...recordBuilderState.addedTags
  ].filter(
    (tag) => !recordBuilderState.droppedTags.includes(tag)
  )
}

/**
 * Returns the list of tags that are related to
 * a record.
 * 
 * `state` is the full system state (as opposed to
 *    the state of just the record builder).
 * `recordId` is the ID of the record.
 */
export function stateToSelectedTags(state) {
  return selectTags(state.recordBuilder)
}

export default recordBuilderReducer;
