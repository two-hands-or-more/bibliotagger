
import { Component } from "react";
import { connect } from "react-redux";

import {
  addLink,
  submitRecord,
  addDescription,
  addTitle,
  toggleTag
} from "./Actions";
import { stateToSelectedTags } from "./Reducer";
import { requestData } from '../tags/Actions';
import { TagPill } from "../tags/TagsList";

/**
 * Defines the UI form where users can add the details
 * for a new record.
 */
class NewRecordForm extends Component {
  componentDidMount() {
    this.props.requestTags();
  }

  render() {
    var tagPills = (
      <p>Tags have not yet been received.</p>
    )
    if(this.props.availableTags !== null) {
      tagPills = this.props.availableTags.map(
        (tag, index) => (
          <TagPill
            linkedToRecord={this.tagIsRelatedToThisRecord(tag)}
            key={ index }
            tag={ tag }
            onClick={ () => this.props.toggleTag(tag.id) }
          />
        )
      );
    }

    return (
      <form>
        <div className="form-group">
          <label htmlFor="newRecordName">
            Name
          </label>
          <input
            type="text"
            className="form-control form-control-sm"
            id="newRecordName"
            placeholder="Name"
            onChange={ (event) => { this.props.addTitle(event.target.value); } }
            value={ this.props.currentNameValue ? this.props.currentNameValue : "" }
          />
        </div>

        <div className="form-group">
          <label htmlFor="newRecordReference">
            Reference link
          </label>
          <input
            type="text"
            className="form-control form-control-sm"
            id="newRecordReference"
            placeholder="URL"
            onChange={ (event) => { this.props.addLink(event.target.value); } }
            value={ this.props.currentUrlValue ? this.props.currentUrlValue : "" }
          />
        </div>

        <div className="form-group">
          <label htmlFor="newRecordDescriptionTextArea" className="form-label">Description</label>
          <textarea
            id="newRecordDescriptionTextArea"
            className="form-control"
            rows="3"
            onChange={ (event) => { this.props.addDescription(event.target.value); } }
            value={ this.props.currentDescription }
          />
        </div>

        { tagPills }
      </form>
    )
  }

  /**
   * Returns true if the record that's being edited is related to
   * the given tag.
   */
  tagIsRelatedToThisRecord = (tag) => {
    // Placeholder until we have storage set up for record tags.
    return this.props.unsavedRelatedTags.includes(tag.id);
  }
}

NewRecordForm = connect(
  (state) => { return {
    recordId: state.recordBuilder.relatesToRecordId,
    currentNameValue: state.recordBuilder.title,
    currentUrlValue: state.recordBuilder.url,
    currentDescription: state.recordBuilder.desc,
    availableTags: state.tags.tags,
    unsavedRelatedTags: stateToSelectedTags(state)
  }},
  {
    addLink,
    addDescription,
    addTitle,
    requestTags: requestData,
    toggleTag
  }
)(NewRecordForm);

/**
 * Decides when to display the record entry form.
 */
class NewRecordModalManager extends Component {
  render() {
    return (
      <div className="modal fade" id="addNewRecordModal" tabIndex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div className="modal-dialog">
          <div className="modal-content">
            <div className="modal-header">
              <h6 className="modal-title" id="exampleModalLabel">Create record</h6>
              <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true"><i className="fas fa-times"/></span>
              </button>
            </div>
            <div className="modal-body">
              <NewRecordForm/>
            </div>
            <div className="modal-footer">
              <button type="button" className="btn btn-secondary" data-dismiss="modal">Close</button>
              <button type="button" className="btn btn-primary"  data-dismiss="modal" onClick={ this.addRecord }>Save changes</button>
            </div>
          </div>
        </div>
      </div>
    )
  }

  /**
   * Submit the form for adding a new record.
   */
  addRecord = () => {
    this.props.submitRecord();
  }
}

export default connect(
  null,
  { submitRecord }
)(NewRecordModalManager);
