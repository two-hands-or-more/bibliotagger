
import { combineReducers } from "redux";
import records from "../records/Reducer";
import errors from "../errors/Reducer";
import recordBuilder from "../newRecord/Reducer";
import tags from "../tags/Reducer";
import topics from "../topics/Reducer"

export default combineReducers({ records, errors, recordBuilder, tags, topics });
