
import {
  REQUEST_DATA,
  GOT_DATA
} from '../tags/Actions';

var initialState = {
  // Are data updates pending?
  waitingForData: false,

  // Tags that can be displayed.
  tags: null
}

function recordsReducer(state=initialState, action) {
  switch (action.type) {
    case REQUEST_DATA:
      return {
        ...state,
        waitingForData: true
      }
    case GOT_DATA:
      return {
        ...state,
        waitingForData: false,
        tags: [...action.payload]
      }
    default:
      return state
  }
}

export default recordsReducer;
