
const ACTION_NS = "tag_data";
export const REQUEST_DATA = `${ACTION_NS}/request_data`;
export const GOT_DATA = `${ACTION_NS}/data_received`;


/**
 * State that the app would like the most recent data set.
 */
export function requestData() {
  return {
    type: REQUEST_DATA
  }
}

/**
 * Announce the new data set to the rest of the app.
 * 
 * `payload` is the list of tags.
 */
export function announceData(payload) {
  return {
    type: GOT_DATA,
    payload
  }
}
