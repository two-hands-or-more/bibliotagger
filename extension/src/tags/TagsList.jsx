
import { Component } from "react";
import { connect } from "react-redux";


/**
 * Display the small pill that gives summary info about a tag.
 */
export class TagPill extends Component {
  render() {
    var {linkedToRecord, tag, onClick} = this.props;

    var additionalStatusDisplay = null;
    if(linkedToRecord) {
      // Build the icon that indicates that the tag will be added.
      // We need to wrap the font-awesome `<i>` tag because font-awesome
      // deletes that tag and replaces it with an <svg>. This confuses and crashes React.
      // Adding a higher-level tag clears up this confusion.
      additionalStatusDisplay = (
        <span><i className="far fa-check-square"/></span>
      );
    }

    return (
      <div>
        <button type="button" className="btn btn-link badge rounded-pill bg-success text-light" onClick={ onClick }>
          { tag.name } { additionalStatusDisplay }
        </button>
      </div>
    )
  }
}


/**
 * Display the full information for a single tag.
 */
class TagDisplay extends Component {
  render() {
    return (
      <TagPill tag={ this.props.tag }/>
    )
  }
}

/**
 * Display the list of available tags.
 */
class TagsList extends Component {
  render() {
    if(this.props.tags) {
      return this._renderData(this.props.tags);
    } else {
      return this._renderNoData();
    }
  }

  /**
   * Display mode when no data have been received yet
   */
  _renderNoData() {
    return (
      <p>No data has been received.</p>
    )
  }

  /**
   * Display mode when data are ready to be displayed.
   * 
   * `tags` is the list of records that should be displayed.
   */
  _renderData(tags) {
    var tagDisplays = tags.map(
      (tag, index) => (<TagDisplay key={ index } tag={ tag }/>)
    );

    return (
      <ul className="list-group">
        {tagDisplays}
      </ul>
    );
  }
}

function mapStateToProps(state) {
  return {
    tags: state.tags.tags
  };
};

export default connect(
  mapStateToProps
)(TagsList);
