
import { screen } from "@testing-library/react";

import { TestEnvironment } from "../../test_utils";
import { TagPill } from "../TagsList";

it("Displays the correct label.", () => {
  let env = new TestEnvironment();

  env.render(
    <TagPill
      tag={{
        name: "the value"
      }}
    />
  );

  const tag = screen.getByText(/the value/i);
  expect(tag).toBeInTheDocument();
});
