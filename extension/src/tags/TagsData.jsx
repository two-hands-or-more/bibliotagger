
import { Component } from "react";
import { connect } from "react-redux";

import { announceData } from "../tags/Actions";
import { announceError } from "../errors/Actions";


/**
 * Decides what actions need to be taken to manage tag data.
 */
class TagsData extends Component {
  render() {
    return (null);
  }

  componentDidMount() {
    this.update();
  }

  componentDidUpdate() {
    this.update();
  }

  /**
   * Check the state store to see what actions need to be taken.
   */
  update = () => {
    if(this.props.waitingForData) {
      // A data request has been detected. Update the data.
      this._getData();
    }
  }

  /**
   * Get the tags that are currently available.
   */
  async _getData() {
    return (
      new Promise((accept) => {
        // Temporary, static tag data.
        accept([
          {
            id: "1",
            name: "todo"
          },
          {
            id: "2",
            name: "reference"
          }
        ])
      })
      .then(this.props.announceData)
      .catch((exc) => {
        this.props.announceError(exc);
        throw exc;
      })
    );
  }
}

function mapStateToProps(state) {
  return {
    waitingForData: state.tags.waitingForData
  };
};

export default connect(
  mapStateToProps,
  { announceData, announceError }
)(TagsData);
