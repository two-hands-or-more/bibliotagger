
import { Fragment } from "react";
import { connect } from "react-redux";

import BasePage from '../app/BasePage';
import { requestData } from '../tags/Actions';
import TagsList from "./TagsList";


/**
 * Contents to be displayed on the Topics page.
 */
class TagsPage extends BasePage {
  componentDidMount() {
    this.props.requestData();
  }

  page() {
    return (
      <Fragment>
        <TagsList/>
      </Fragment>
    );
  }
}


export default connect(
  null,
  { requestData }
)(TagsPage);
