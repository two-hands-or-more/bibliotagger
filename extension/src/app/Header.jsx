
import { NavLink, Link } from "react-router-dom";


/**
 * Default header for pages.
 */
export default function Header(props) {
  return (
    <nav className="navbar navbar-expand-lg navbar-dark bg-dark">
      <Link className="navbar-brand" to="/">BiblioTag</Link>
      <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span className="navbar-toggler-icon"></span>
      </button>

      <div className="collapse navbar-collapse" id="navbarSupportedContent">
        <ul className="navbar-nav mr-auto">
          <NavLink className="nav-link" activeClassName="active" to="/records">Records</NavLink>
          <NavLink className="nav-link" activeClassName="active" to="/tags">Tags</NavLink>
          <NavLink className="nav-link" activeClassName="active" to="/topics">Topics</NavLink>
        </ul>
      </div>
    </nav>
  );
}
