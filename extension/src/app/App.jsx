
import { Fragment } from "react";
import {
  BrowserRouter as Router,
  Route,
  Switch
} from "react-router-dom";

import TagsData from "../tags/TagsData";
import RecordsPage from "../records/RecordsPage";
import TagsPage from "../tags/TagsPage";
import TopicsPage from "../topics/TopicsPage";
import RecordsData from "../records/RecordsData";
import './App.css';

function App() {
  return (
    <Fragment>
      <RecordsData/>
      <TagsData/>

      <Router>
        <Switch>
          <Route path="/topics">
            <TopicsPage/>
          </Route>
          <Route path="/records">
            <RecordsPage/>
          </Route>
          <Route path="/tags">
            <TagsPage/>
          </Route>
          <Route path="/">
            <RecordsPage/>
          </Route>
        </Switch>
      </Router>
    </Fragment>
  );
}

export default App;
