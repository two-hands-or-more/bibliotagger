
import { Component, Fragment } from "react";

import Header from './Header';
import { NotImplementedError } from "../errors/Exceptions";
import ErrorReport from '../errors/ErrorReport';

/**
 * Base template page that defines the content for every
 * page that's displayed.
 */
export default class BasePage extends Component {
  render() {
    var page_contents = this.page();

    return (
      <Fragment>
        <Header/>
        
        <main className="container-fluid AppMinimums mt-3" role="main">
          <ErrorReport/>

          { page_contents }
        </main>
      </Fragment>
    )
  }

  /**
   * Render the content that is specific to this page.
   */
  page() {
    throw new NotImplementedError(`Method page() is not implemented by ${this.constructor.name}.`)
  }
}