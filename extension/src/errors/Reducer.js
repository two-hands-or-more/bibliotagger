
import { NEW_ERROR } from "./Actions";

var initialState = {
  // List of errors that have been encountered.
  errors: []
}

function errorsReducer(state=initialState, action) {
  switch (action.type) {
    case NEW_ERROR:
      return {
        ...state,
        errors: [...state.errors, action.payload]
      }
    default:
      return state
  }
}

export default errorsReducer;
