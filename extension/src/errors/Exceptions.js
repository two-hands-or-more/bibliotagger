
/**
 * Base error for all exceptions explicitly thrown by this extension.
 */
export class BiblioTaggerError extends Error {};


/**
 * Thrown when a method is not implemented.
 */
export class NotImplementedError extends BiblioTaggerError {};


/**
 * Thrown when something tries to change a record's ID.
 */
export class IllegalRename extends BiblioTaggerError {};


/**
 * Thrown when something tries to parse data that's missing a
 * required field.
 */
export class RequiredFieldMissing extends BiblioTaggerError {};

/**
 * Thrown when an assumption made in our code doesn't hold.
 */
export class AssertionError extends BiblioTaggerError {};


/**
 * Thrown when an a duplicate key is detected.
 */
export class DuplicateIdError extends BiblioTaggerError {};
