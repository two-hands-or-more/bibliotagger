
import { screen } from "@testing-library/react"

import { TestEnvironment } from "../../test_utils";
import ErrorReport from "../ErrorReport";
import { announceError } from "../Actions";


it("Doesn't display when there are no errors", () => {
  let env = new TestEnvironment();

  env.render(
    <ErrorReport/>
  );

  const errorMessage = screen.queryByText(/Error!/i);
  expect(errorMessage).toBeNull();
});


it("Does display when there are errors", () => {
  let env = new TestEnvironment();
  env.store.dispatch(announceError("meh"));

  env.render(
    <ErrorReport/>
  );

  const errorMessage = screen.getByText(/Error!/i);
  expect(errorMessage).toBeInTheDocument();
});
