
import { announceError } from "../Actions";


it("Passes the correct payload", () => {
  let expected = "a value";
  let result = announceError(expected);

  expect(result).toEqual(result);
});
