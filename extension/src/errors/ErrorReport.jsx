
import { Component } from "react";
import { connect } from "react-redux";


/**
 * Defines how errors should be displayed to the end
 * user.
 */
class ErrorReport extends Component {
  render() {
    if(this.props.errors.length > 0) {
      return (
        <div className="alert alert-danger" role="alert">
          Error!
        </div>
      );
    }

    return (null);
  }
}

function mapStateToProps(state) {
  return {
    errors: state.errors.errors
  };
};

export default connect(
  mapStateToProps
)(ErrorReport);