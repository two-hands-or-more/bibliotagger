
const ACTION_NS = "errors";
export const NEW_ERROR = `${ACTION_NS}/new_error`;


/**
 * State that a new error was encountered.
 * 
 * `error` is the exception that was thrown.
 */
export function announceError(error) {
  return {
    type: NEW_ERROR,
    payload: {
      payload: error
    }
  }
}
