
import { Component } from "react";


/**
 * Display widget for topic pills.
 */
export default class TopicPill extends Component {
  render() {
    var { name } = this.props;

    return (
      <button type="button" className="btn btn-link badge rounded-pill bg-success text-light">
        { name }
      </button>
    )
  }
}
