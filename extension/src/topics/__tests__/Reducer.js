
import { TestEnvironment } from "../../test_utils";
import { announceTopics } from "../Actions";
import { DuplicateIdError } from "../../errors/Exceptions";


it("Initially sets the list of topics to `null`.", () => {
  let env = new TestEnvironment();

  expect(env.store.getState().topics.topics).toBeNull();
});


it("Can add initial list of topics", () => {
  let env = new TestEnvironment();

  env.store.dispatch(announceTopics(
    [
      {id: "meh", name: "meh"},
      {id: "meh2", name: "meh"}
    ]
  ));

  expect(env.store.getState().topics.topics).not.toBeNull();
  expect(Object.keys(env.store.getState().topics.topics).length).toEqual(2);
});


it("Returns a new index when we announce the topics", () => {
  let originalTopicIndex = {}

  let env = new TestEnvironment({
    topics: {
      topics: originalTopicIndex
    }
  });

  env.store.dispatch(announceTopics(
    [{id: "meh", name: "meh"}],
  ));

  expect(env.store.getState().topics.topics).not.toBe(originalTopicIndex);
});


it("Can avoid updating state if the list of topics is empty.", () => {
  let originalTopicIndex = {}

  let env = new TestEnvironment({
    topics: {
      topics: originalTopicIndex
    }
  });

  env.store.dispatch(announceTopics(
    []
  ));

  expect(env.store.getState().topics.topics).toBe(originalTopicIndex);
});


it("Throws an error if the ID is in the list multiple times.", () => {
  let env = new TestEnvironment();

  let addBadTopicsList = () => env.store.dispatch(announceTopics(
    [
      {id: "duplicate_id", name: "meh"},
      {id: "duplicate_id", name: "meh"}
    ]
  ));

  expect(addBadTopicsList).toThrowError(DuplicateIdError);
});
