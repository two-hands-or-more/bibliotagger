
import {
  requestTopics,
  announceTopics,
  DATA_REQUESTED,
  ANNOUNCE_TOPICS
} from "../Actions";
import { TestEnvironment } from "../../test_utils";
import { STATUS_OPTS } from "../Reducer";


describe("Topic request action", () => {
  it("Sets the state to pending", () => {
    const env = new TestEnvironment();

    env.store.dispatch(requestTopics());
    expect(env.store.getState().topics.status).toEqual(STATUS_OPTS.requested)
  })
});


describe("Topic announce action", () => {
  it("Returns the correct event.", () => {
    const val = announceTopics();

    expect(val.type).toEqual(ANNOUNCE_TOPICS);
  });

  it("Sets the state to complete", () => {
    const env = new TestEnvironment();

    env.store.dispatch(announceTopics([]));
    expect(env.store.getState().topics.status).toEqual(STATUS_OPTS.complete)
  })
});
