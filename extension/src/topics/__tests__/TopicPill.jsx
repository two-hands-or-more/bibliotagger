
import { screen } from "@testing-library/react";

import { TestEnvironment } from "../../test_utils";
import TopicPill from "../TopicPill";

it("Displays the correct label.", () => {
  let env = new TestEnvironment();

  env.render(
    <TopicPill
      name="the name"
    />
  );

  const tag = screen.getByText(/the name/i);
  expect(tag).toBeInTheDocument();
});
