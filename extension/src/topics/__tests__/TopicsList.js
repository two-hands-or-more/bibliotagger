
import { screen } from "@testing-library/react";

import { TestEnvironment } from "../../test_utils";
import TopicsList from "../TopicsList";
import { connectComponent } from "../TopicsList";
import * as Actions from "../Actions";


it("Renders no data when no data have been received and no request has been sent.", () => {
  const env = new TestEnvironment();

  // Replace the topics getter with a no-op so that we can see what's displayed
  // if data collection doesn't happen.
  jest.spyOn(Actions, "getTopics").mockImplementation(() => {return {"type": "noop"}})
  const TopicsList = connectComponent();

  env.render(
    <TopicsList/>
  );

  const loading_text = screen.getByText(/No topics/i);
  expect(loading_text).toBeInTheDocument();
});


it("Requests topic data when it renders", () => {
  const env = new TestEnvironment();

  const spy = jest.spyOn(Actions, "getTopics");
  const TopicsList = connectComponent();

  env.render(
    <TopicsList/>
  );

  expect(spy).toHaveBeenCalled();
});


it("Renders a loading message when waiting for data.", () => {
  const env = new TestEnvironment();

  // Replace the topics announcer with a no-op so that we can see what's displayed
  // if data collection doesn't return quickly return a value.
  jest.spyOn(Actions, "announceTopics").mockImplementation(() => {return {"type": "noop"}})
  const TopicsList = connectComponent();

  env.render(
    <TopicsList/>
  );

  const loading_text = screen.getByText(/Loading/i);
  expect(loading_text).toBeInTheDocument();
});


it("Renders the data display when data are available.", async () => {
  const env = new TestEnvironment();

  env.render(
    <TopicsList/>
  );

  // No-op call so that promise chains resolve.
  // Thanks: https://stackoverflow.com/a/52667364
  await Promise.resolve();

  const text = screen.getByText(/Topics list/i);
  expect(text).toBeInTheDocument();
});
