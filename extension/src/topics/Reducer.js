
import { DATA_REQUESTED, ANNOUNCE_TOPICS } from "./Actions";
import { DuplicateIdError } from "../errors/Exceptions";


export const STATUS_OPTS = {
  idle: "IDLE",
  requested: "REQ",
  complete: "COMPLETE",
  error: "ERROR"
}


var initialState = {
  // Status of the request for this data.
  status: STATUS_OPTS.idle,
  // Topics that can be displayed.
  topics: null
}


/**
 * Serialize the data for the given topic and index
 * it into the store.
 */
function serializeTopic(topicsIndex, { id, name }={}) {
  if(id in topicsIndex) {
    throw new DuplicateIdError(`Cannot index. ID '${id}' is used multiple times.`);
  }

  topicsIndex[id] = { id, name };
  return topicsIndex;
}


function topicsReducer(state=initialState, action) {
  switch (action.type) {
    case DATA_REQUESTED:
      return {
        ...state,
        status: STATUS_OPTS.requested
      }
    case ANNOUNCE_TOPICS:
      let currTopics = state.topics ? state.topics : {};
      let topicsToUse = currTopics;
      if(action.topics.length !== 0) {
        // More topics are available. Create a new index.
        topicsToUse = {...action.topics.reduce(serializeTopic, currTopics)};
      }

      return {
        ...state,
        topics: topicsToUse,
        status: STATUS_OPTS.complete
      }
    default:
      return state
  }
}

export default topicsReducer;
