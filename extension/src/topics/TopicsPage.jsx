
import BasePage from "../app/BasePage";
import TopicsList from "./TopicsList";


/**
 * Contents to be displayed on the Topics page.
 */
export default class TopicsPage extends BasePage {
  page() {
    return (
      <TopicsList/>
    );
  }
}
