
const ACTION_NS = "topics";
export const DATA_REQUESTED = `${ACTION_NS}/topics_requested`;
export const ANNOUNCE_TOPICS = `${ACTION_NS}/topics_announced`;


/**
 * Announce that something has asked for Topics data.
 */
export function requestTopics() {
  return {
    type: DATA_REQUESTED
  }
}


/**
 * Gather data for the topics.
 */
export function getTopics() {
  return async (dispatch) => {
    let fakeAsyncDataGetter = new Promise((accept) => {
      accept([{
        id: "1",
        name: "topic 1"
      }]);
    });

    dispatch(requestTopics());
    let data = await fakeAsyncDataGetter;
    dispatch(announceTopics(data));
  }
}


/**
 * Announce the new list of topics
 * 
 * `topics` is a list of topics.
 */
export function announceTopics(topics) {
  return {
    type: ANNOUNCE_TOPICS,
    topics
  }
}
