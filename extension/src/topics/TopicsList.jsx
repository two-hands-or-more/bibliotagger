
import { Component } from "react";
import { connect } from "react-redux";

import { getTopics } from "./Actions";
import { STATUS_OPTS } from "./Reducer";


/**
 * Defines how to display lists of topics to the user.
 */
export class _TopicsList extends Component {
  componentDidMount() {
    this.props.getTopics();
  }

  render() {
    switch(this.props.topicDataStatus) {
      case STATUS_OPTS.requested:
        return this._renderLoadingScreen();
      
      case STATUS_OPTS.idle:
      case STATUS_OPTS.complete:
        if(this.props.topics === null || Object.keys(this.props.topics) == 0) {
          return this._renderNoData();
        } else {
          return this._renderContent();
        }
    }
  }

  /**
   * Content to display when we don't have a topics list.
   */
  _renderNoData() {
    return "No topics are currently available.";
  }

  /**
   * Display progress on loading the data we requested.
   */
  _renderLoadingScreen() {
    return "Loading available topics.";
  }

  /**
   * Display the list of topics
   */
  _renderContent() {
    return "Topics list";
  }
}


function mapStateToProps(state) {
  return {
    topics: state.topics.topics,
    topicDataStatus: state.topics.status
  }
}


/**
 * Allow connection to be deferred to later.
 * 
 * By default, we immediately export the connected component, just
 * like traditional react-redux. However, the problem with this approach
 * is it doesn't let Jest spy on action creators. By deferring connection,
 * we can mock/spy the action creator functions, and _then_ connect them
 * to the component.
 */
export function connectComponent() {
  return connect(
    mapStateToProps,
    { getTopics }
  )(_TopicsList);
}


export default connectComponent();
