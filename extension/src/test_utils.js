
import React from "react";
import { render as rtlRender } from "@testing-library/react";
import { createStore, applyMiddleware } from "redux";
import { Provider } from "react-redux";
import thunk from 'redux-thunk';

import reducer from "./redux/reducers"

/**
 * Defines a disposable environment that can be used
 * to more easily render tests.
 */
export class TestEnvironment {
  /**
   * `initialState` is an object defining the initial state
   *    that should be loaded into the new store.
   */
  constructor(initialState) {
    this.store = createStore(reducer, initialState, applyMiddleware(thunk));
  }

  /**
   * Customize the `render` function so that we can more easily write tests
   * for react-redux code. Thanks: https://redux.js.org/recipes/writing-tests#components
   */
  render = (ui, renderOptions) => {
    let me = this;
    function Wrapper({ children }) {
      return <Provider store={me.store}>{children}</Provider>
    }
    return rtlRender(ui, { wrapper: Wrapper, ...renderOptions })
  }
}


/**
 * Make sure we clear all mocks after every test.
 */
afterEach(() => {
  jest.restoreAllMocks();
});
