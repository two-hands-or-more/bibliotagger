
/**
 * Defines how to display UUIDs.
 */
export default function UUID({ uuid }) {
  return (
    <div className="UUID">
      { uuid }
    </div>
  )
};
