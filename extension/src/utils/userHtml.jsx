
import sanitizeHtml from "sanitize-html";


/**
 * Defines how to sanitize HTML that's provided by the user. This
 * is important when displaying links, markdown, etc.
 * 
 * Thanks to [this](https://stackoverflow.com/a/38663813/5046197)
 * for this approach.
 */
const defaultOptions = {
  allowedTags: [ "b", "i", "em", "strong", "a" ],
  allowedAttributes: {
    "a": [
      "href",
      {
        "name": "target",
        "values": ["_blank"],
      },
      {
        "name": "rel",
        "values": ["noreferrer noopener"],
      }
    ]
  }
};
const _sanitize = (dirty, options) => ({
  __html: sanitizeHtml(
    dirty,
    { ...defaultOptions, ...options }
  )
});
const UserHtml = ({ html, options }) => (
  <span dangerouslySetInnerHTML={_sanitize(html, options)} />
);

export default UserHtml;
