
import { IllegalRename, RequiredFieldMissing } from "../errors/Exceptions";


/**
 * Holds data about a single record and provides
 * methods for parsing to and from different formats.
 */
export default class Record {
  constructor(data) {
    this._fromJson(data)
  }

  /**
   * Reset the fields of this instance based on the contents
   * of the given JSON.
   * 
   * This method should typically only be used by the constructor. If
   * you're wanting to update a record's field, just create a new record
   * instance.
   */
  _fromJson(json) {
    if(!("title" in json)) {
      throw new RequiredFieldMissing(
        "Cannot parse record from JSON. Missing field `title`."
      )
    }
    this.title = json.title;

    // Get URL
    this.url = json.url;

    // If an id is specified, use that value. If not, default to `null`.
    this._id = "id" in json ? json.id : null;

    // Get description
    this.desc = json.desc;

    // Get the list of tags that are associated with this record.
    this.selectedTags = json.selectedTags ? json.selectedTags : [];
  }

  /**
   * Serialize the object to the format that should be stored long-term.
   */
  toJson() {
    return {
      title: this.title,
      id: this._id,
      url: this.url,
      desc: this.desc,
      selectedTags: this.selectedTags
    };
  }

  /**
   * Return the unique identifier for this object.
   */
  getId() {
    return this._id;
  }

  /**
   * Set the unique identifier for this object.
   * 
   * Identifiers can only be set once. An error will be thrown if
   * this instance already has an ID.
   * 
   * `newId` is the ID that should be set for this instance.
   */
  setId(newId) {
    var currId = this.getId();
    if(currId !== null) {
      throw new IllegalRename(
        `Cannot change IDs. This record already has its ID set to ${currId}.`
      )
    }

    this._id = newId;
  }
}
