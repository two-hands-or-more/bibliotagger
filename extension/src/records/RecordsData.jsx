
import { Component } from "react";
import { connect } from "react-redux";

import { announceData, requestData, delDone } from "../records/Actions";
import { announceError } from "../errors/Actions";
import { confirmSubmission } from "../newRecord/Actions";
import { selectTags } from "../newRecord/Reducer";
import chooseInterface from "../api/interfaceFactory.js";
import Record from "../utils/record";


/**
 * Convert the data prepper to a Record instance.
 * 
 * `dataPrep` is the current data stored in the data preparer.
 * 
 * Returns a new `Record` instance.
 */
function dataPrepToRecord(dataPrep) {
  return new Record({
    title: dataPrep.title,
    id: dataPrep.relatesToRecordId,
    url: dataPrep.url,
    desc: dataPrep.desc,
    selectedTags: selectTags(dataPrep, dataPrep.relatesToRecordId)
  });
}


/**
 * Gather the list of records.
 */
class RecordsData extends Component {
  render() {
    return (null);
  }

  componentDidMount() {
    this.update();
  }

  componentDidUpdate() {
    this.update();
  }

  /**
   * Check the state store to see what actions need to be taken.
   */
  update = () => {
    if(this.props.waitingForData) {
      // A data request has been detected. Update the data.
      this._getData();
    }

    if(this.props.recordReadyForSubmission) {
      // We've received a request to submit new data to the persistent data
      // store.
      this._submitData();
    }

    if(this.props.recordToDelete) {
      // A record needs to be removed from the persistent data store.
      this._delRecord();
    }
  }

  /**
   * Get data that should be displayed.
   */
  _getData = async () => {
    return (
      chooseInterface()
      .getRecords()
      .then(this.props.announceData)
      .catch((exc) => {
        this.props.announceError(exc);
        throw exc;
      })
    )
  }

  /**
   * Update the persisted data with the new prepared data.
   */
  _submitData = async () => {
    if(this.props.preppedRecord.relatesToRecordId === null) {
      // The data in the preparer isn't related to an existing record.
      // Create a new record.
      this._newData();
    } else {
      // We're editing an existing record.
      this._updateData();
    }
  }

  /**
   * Upload new data to the persistent store.
   */
  _newData = async () => {
    // Submit the data.
    return (
      new Promise((accept) => {
        var preppedRecord = dataPrepToRecord(this.props.preppedRecord)

        // State that we've collected the data we need and that the UI
        // can clear itself.
        this.props.confirmSubmission();

        accept(preppedRecord);
      })
      .then(chooseInterface().addRecord)
      .then(this.props.requestData)
      .catch((exc) => {
        this.props.announceError(exc);
        throw exc;
      })
    )
  }

  /**
   * Update a record that's already in the server.
   */
  _updateData = async () => {
    // Submit the data.
    return (
      new Promise((accept) => {
        var preppedRecord = dataPrepToRecord(this.props.preppedRecord)

        // State that we've collected the data we need and that the UI
        // can clear itself.
        this.props.confirmSubmission();

        accept(preppedRecord);
      })
      .then(chooseInterface().updateRecord)
      .then(this.props.requestData)
      .catch((exc) => {
        this.props.announceError(exc);
        throw exc;
      })
    )
  }

  /**
   * Delete the requested record.
   */
  _delRecord = async () => {
    chooseInterface()
    .delRecord(this.props.recordToDelete)
    .then(this.props.delDone)
    .then(this.props.requestData)
    .catch((exc) => {
      this.props.announceError(exc);
      throw exc;
    })
  }
}

function mapStateToProps(state) {
  return {
    waitingForData: state.records.waitingForData,
    recordReadyForSubmission: state.recordBuilder.readyForSubmission,
    preppedRecord: state.recordBuilder,
    recordToDelete: state.records.recordToDelete,
  };
};

export default connect(
  mapStateToProps,
  {
    announceData,
    announceError,
    confirmSubmission,
    requestData,
    delDone
  }
)(RecordsData);
