
import { Component } from "react";
import { connect } from "react-redux";

import UserHtml from "../utils/userHtml";
import UUID from "../utils/uuid";
import { delRecord } from "./Actions";
import {
  relateTo,
  addLink,
  addDescription,
  addTitle,
  loadTags
} from "../newRecord/Actions";
import { TagPill } from "../tags/TagsList";
import { AssertionError } from "assert";

// Number of characters of a URL to display before
// shortening it.
const MAX_CHARS_PER_SHORT_URL = 12;

/**
 * Display a single record.
 */
class RecordDisplay extends Component {
  render() {
    // Build the URL that should be displayed
    var shortUrl = this.props.record.url;
    if(shortUrl && shortUrl.length > 8) {
      shortUrl = shortUrl.substring(0, MAX_CHARS_PER_SHORT_URL) + "..."
    }

    // Build the list of tags that should be displayed.
    var tagPills = (
      this.props.record.selectedTags
      .map((tagId) => {
        var tagObjectsMatchingId = this.props.allTags.filter((currTag) => currTag.id === tagId);
        if(tagObjectsMatchingId.length > 1) {
          throw AssertionError("More than one tag was selected based on the given ID.")
        } else if(tagObjectsMatchingId.length === 0) {
          throw AssertionError("Could not find tag matching requested ID.")
        }

        return tagObjectsMatchingId[0];
      })
      .map((tag, index) => (
          <TagPill
            key={ index }
            tag={ tag }
          />
      ))
    )

    return (
      <li className="list-group-item">
        <div className="row">
          <div className="col-8">
            <h5>
              { this.props.record.title }
            </h5>
          </div>
          <div className="col-4">
            <button type="button" className="btn btn-link float-right" onClick={ this._requestDelete }>
              <i className="fas fa-times"/>
            </button>
            <button
              type="button"
              onClick={ this._edit }
              className="btn btn-link float-right"
              data-toggle="modal"
              data-target="#addNewRecordModal"
            >
              <i className="far fa-edit"/>
            </button>
          </div>
        </div>

        <div className="row">
          <div className="col-lg-12">
            Reference: <UserHtml
              html={ `<a href=${ this.props.record.url } target="_blank" rel="noopener noreferrer">${ shortUrl }</a>` }
            />
          </div>
        </div>

        <hr/>

        <div className="row">
          <div className="col-lg-12">
            <p>{ this.props.record.desc }</p>
          </div>
        </div>

        <div className="row">
          <div className="col-lg-12">
            <small className="float-right">
              <UUID uuid={ this.props.record.getId() }/>
            </small>
          </div>
        </div>

        <div className="row">
          <div className="col-lg-12">
            { tagPills }
          </div>
        </div>
      </li>
    )
  }

  /**
   * Ask to be deleted.
   */
  _requestDelete = () => {
    this.props.delRecord(this.props.record.getId());
  }

  /**
   * Ask to be edited.
   */
  _edit = () => {
    var id = this.props.record.getId();

    this.props.relateTo(id);
    this.props.addLink(this.props.record.url);
    this.props.addDescription(this.props.record.desc)
    this.props.addTitle(this.props.record.title)
    this.props.loadTags(this.props.record.selectedTags)
  }
}

RecordDisplay = connect(
  (state) => { return {
    allTags: state.tags.tags
  }},
  { delRecord, relateTo, addLink, addDescription, addTitle, loadTags }
)(RecordDisplay);


/**
 * Display the list of records.
 */
class RecordsList extends Component {
  render() {
    if(this.props.records) {
      return this._renderData(this.props.records);
    } else {
      return this._renderNoData();
    }
  }

  /**
   * Display mode when no data have been received yet
   */
  _renderNoData() {
    return (
      <p>No data has been received.</p>
    )
  }

  /**
   * Display mode when data are ready to be displayed.
   * 
   * `records` is the list of records that should be displayed.
   */
  _renderData(records) {
    var recordDisplays = records.map(
      (record, index) => (<RecordDisplay key={ index } record={ record }/>)
    );

    return (
      <ul className="list-group">
        {recordDisplays}
      </ul>
    );
  }
}

function mapStateToProps(state) {
  return {
    records: state.records.records
  };
};

export default connect(
  mapStateToProps
)(RecordsList);
