
import { Fragment } from "react";
import { connect } from "react-redux";

import BasePage from "../app/BasePage";
import { requestData } from "./Actions";
import RecordsList from "./RecordsList";
import NewRecordModalManager from "../newRecord/newRecord";
import { clearForm } from "../newRecord/Actions";


/**
 * Contents that should be displayed on the tags page.
 */
class RecordsPage extends BasePage {
  componentDidMount() {
    this.props.requestData();
  }

  page() {
    return (
      <Fragment>
        <div className="row mt-3">
          <div className="col-12">
            <div className="row float-right">
              <div className="col-lg-12">
                <button
                  type="button"
                  className="btn btn-primary"
                  data-toggle="modal"
                  data-target="#addNewRecordModal"
                  onClick={ this._clearPrep }
                >
                  Add new record
                </button>
                <NewRecordModalManager/>
              </div>
            </div>
          </div>
        </div>

        <div className="row mt-3">
          <div className="col-lg-12">
            <RecordsList/>
          </div>
        </div>
      </Fragment>
    );
  }

  /**
   * Clear current entries in the data prep form.
   */
  _clearPrep = () => {
    this.props.clearForm()
  }
}


export default connect(
  null,
  { requestData, clearForm }
)(RecordsPage);
