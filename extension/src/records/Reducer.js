
import {
  REQUEST_DATA,
  GOT_DATA,
  DEL,
  DEL_DONE
} from './Actions';

var initialState = {
  // Are data updates pending?
  waitingForData: false,

  // Will be a list of records.
  records: null,

  // If we're waiting for a record to be deleted, this will
  // be set to the ID of that record.
  recordToDelete: null
}

function recordsReducer(state=initialState, action) {
  switch (action.type) {
    case REQUEST_DATA:
      return {
        ...state,
        waitingForData: true
      }
    case GOT_DATA:
      return {
        ...state,
        waitingForData: false,
        records: [...action.payload]
      }
    case DEL:
      return {
        ...state,
        recordToDelete: action.payload
      }
    case DEL_DONE:
      return {
        ...state,
        recordToDelete: null
      }
    default:
      return state
  }
}

export default recordsReducer;
