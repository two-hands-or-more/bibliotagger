
import { screen } from "@testing-library/react";

import { TestEnvironment } from "../../test_utils";
import RecordsData from "../RecordsData";

/**
 * When we load the initial page, we don't want to load the data
 * until something explicitly asks for an update.
 */
it("No data are loaded initially", () => {
  let env = new TestEnvironment();

  env.render(
    <RecordsData/>
  );

  expect(env.store.getState().records.waitingForData).toBe(false);
});
