
const ACTION_NS = "record_data";
export const REQUEST_DATA = `${ACTION_NS}/request_data`;
export const GOT_DATA = `${ACTION_NS}/data_received`;
export const DEL = `${ACTION_NS}/delete_record`;
export const DEL_DONE = `${ACTION_NS}/delete_record_done`;


/**
 * State that the app would like the most recent data set.
 */
export function requestData() {
  return {
    type: REQUEST_DATA
  }
}

/**
 * Announce the new data set to the rest of the app.
 * 
 * `payload` is the list of this user's records.
 */
export function announceData(payload) {
  return {
    type: GOT_DATA,
    payload
  }
}

/**
 * State that a record should be deleted.
 * 
 * `payload` is the ID of the record that should be removed.
 */
export function delRecord(payload) {
  return {
    type: DEL,
    payload
  }
}

/**
 * State that the request to delete the record has been received
 * and that it will be performed.
 */
export function delDone() {
  return {
    type: DEL_DONE
  }
}
