/*global chrome*/

import PlaceholderInterface from './placeholder';
import ChromeExtensionInterface from './chromeExtension';


/**
 * Decide which `BaseInterface` implementation is appropriate for
 * this environment.
 */
export default function chooseInterface() {
  if(window.chrome && chrome.runtime && chrome.runtime.id) {
    // We're in Chrome and we're loaded into a plugin.
    // Thanks https://stackoverflow.com/a/22563123
    return new ChromeExtensionInterface();
  }

  // Default case: Just make up fake data.
  return new PlaceholderInterface();
}
