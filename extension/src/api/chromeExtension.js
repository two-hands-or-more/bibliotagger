/*global chrome*/

import BaseInterface from "./base";


/**
 * Defines how to interact with Chrome's extension API.
 */
export default class ChromeExtensionInterface extends BaseInterface {
  /**
   * Defines how this interface should get data from a particular key.
   * 
   * `key` is the name of the key that should be loaded.
   */
  getKey = async (key) => {
    var rawData = await new Promise((accept) => {
      chrome.storage.sync.get(key, accept)
    });

    return rawData[key];
  }

  /**
   * Defines how this interface should save data to a particular key.
   * 
   * `key` is the name of the key that should be written to.
   * `data` are the data (JSON) that should be written to this key.
   */
  saveKey = async (key, data) => {
    // Add the new record to the existing set of records.
    await new Promise((accept) => {
      var setThis = {
        [key]: data
      }
      chrome.storage.sync.set(
        setThis,
        accept
      );
    });
  }
}
