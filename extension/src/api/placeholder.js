
import BaseInterface from "./base";

/**
 * In-memory storage location to simulate storing data.
 */
var fakeDataStore = {
}


/**
 * Defines simple test data that can be used for development
 * and testing outside of the browser's plugin environment.
 */
export default class PlaceholderInterface extends BaseInterface {
  /**
   * Defines how this interface should get data from a particular key.
   * 
   * `key` is the name of the key that should be loaded.
   */
  getKey = async (key) => {
    return new Promise((accept) => {
      accept(fakeDataStore[key])
    });
  }

  /**
   * Defines how this interface should save data to a particular key.
   * 
   * `key` is the name of the key that should be written to.
   * `data` are the data (JSON) that should be written to this key.
   */
  saveKey = async (key, data) => {
    await new Promise((accept) => {
      fakeDataStore[key] = data
      accept();
    });
  }
}
