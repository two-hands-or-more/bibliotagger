
import { v4 as uuidv4 } from 'uuid';

import { NotImplementedError, AssertionError } from "../errors/Exceptions";
import Record from "../utils/record";


/**
 * Base class that defines the methods that need to be implemented
 * to allow this extension to load and save its data.
 */
export default class BaseInterface {
  /**
   * Return the list of records that can be displayed.
   */
  getRecords = async () => {
    var data = await this.getKey("records");

    if(data === undefined) {
      // No data have been saved yet. Just
      // return an empty list.
      return [];
    }

    // Parse the JSON data into useful objects, then return
    // the list
    return data.map((jsonData) => new Record(jsonData));
  }

  /**
   * Add (persist/save) a new record.
   */
  addRecord = async (record) => {
    // If this record doesn't already have an ID (i.e.,
    // it's a new record), then generate and set a new ID.
    if(!record.getId()) {
      record.setId(this.generateId());
    }

    var newRecordList = [...await this.getRecords(), record];

    return this.saveKey("records", newRecordList.map((record) => record.toJson()));
  }

  /**
   * Delete the given record from the persistent store.
   */
  delRecord = async (recordId) => {
    // Select the record that should be removed.
    var recordList = await this.getRecords();
    var newRecordList = (
      recordList
      .filter((record) => record.getId() !== recordId)
    )

    return this.saveKey("records", newRecordList.map((record) => record.toJson()));
  }

  /**
   * Update the given record in the persistent store.
   */
  updateRecord = async (record) => {
    // Get the ID of the record that should be updated.
    var idToUpdate = record.getId();
    if(idToUpdate === null) {
      throw new AssertionError("Attempted to update a record, but the input data doesn't specify an ID.")
    }

    // Select the record that should be updated.
    var recordList = await this.getRecords();
    var indexOfRecordToUpdate = (
      recordList
      .map((record) => record.getId())
      .indexOf(idToUpdate)
    )

    // Remove the old record from the list and put the new one
    // in its place.
    var newRecordList = [...recordList];
    newRecordList[indexOfRecordToUpdate] = record;

    return this.saveKey("records", newRecordList.map((record) => record.toJson()));
  }

  /**
   * Defines how this interface should get data from a particular key.
   * 
   * `key` is the name of the key that should be loaded.
   */
  getKey = async (key) => {
    throw new NotImplementedError(`Method getKey() is not implemented by ${this.constructor.name}.`)
  }

  /**
   * Defines how this interface should save data to a particular key.
   * 
   * `key` is the name of the key that should be written to.
   * `data` are the data (JSON) that should be written to this key.
   */
  saveKey = async (key, data) => {
    throw new NotImplementedError(`Method saveKey() is not implemented by ${this.constructor.name}.`)
  }

  /**
   * Generate a new unique identifier.
   */
  generateId() {
    return uuidv4();
  }

  /**
   * Get the list of topics.
   */
  getTopics = async () => {
    var data = await this.getKey("topics");

    if(data === undefined) {
      // No data have been saved yet. Just
      // return an empty list.
      return [];
    }

    return data;
  }
}
