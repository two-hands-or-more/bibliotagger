
import PlaceholderInterface from "../placeholder";


describe("getTopics", () => {
  it("Returns the correct empty response when there are no topics.", async () => {
    const iface = new PlaceholderInterface();

    let initialTopics = await iface.getTopics();

    expect(initialTopics).toEqual([]);
  });

  it("Returns the correct list of results.", async () => {
    const iface = new PlaceholderInterface();

    const expected = [
      {id: "id1"},
      {id: "id2"}
    ];
    jest.spyOn(iface, "getKey").mockImplementation(() => {
      return Promise.resolve(expected);
    });

    let topics = await iface.getTopics();
    expect(topics).toEqual(expected);
  });
})
